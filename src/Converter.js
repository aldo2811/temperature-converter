import React, {Component} from 'react';
import InputBox from "./InputBox";

class Converter extends Component {

  state = {
    currentScale: "",
    celsius: "",
    fahrenheit: "",
    kelvin: ""
  };

  handleChange = (e) => {
    const value = parseFloat(e.target.value);
    const currentScale = e.target.name;
    this.convert(currentScale, value);
  };

  convert = (scale, value) => {
    let celsius, fahrenheit, kelvin;
    switch(scale) {
      case 'Celsius':
        celsius= value;
        fahrenheit = this.cToF(value);
        kelvin = this.cToK(value);
        break;
      case 'Fahrenheit':
        celsius= this.fToC(value);
        fahrenheit = value;
        kelvin = this.fToK(value);
        break;
      case 'Kelvin':
        celsius= this.kToC(value);
        fahrenheit = this.kToF(value);
        kelvin = value;
        break;
    }
    this.setState({celsius, fahrenheit, kelvin, currentScale: scale});
  };

  cToK = value => {
    return value + 273.15;
  };

  cToF = value => {
    return (9/5 * value) + 32;
  };

  fToC = value => {
    return (value - 32) * 5/9;
  };

  fToK = value => {
    return (value - 32) * 5/9 + 273.15;
  };

  kToC= value => {
    return value - 273.15;
  };

  kToF = value => {
    return (9/5 * (value - 273.15)) + 32;
  };

  displayValue = (value) => {
    if (!isNaN(value)) {
      return value;
    }
    else {
      return "";
    }
  };

  render() {
    return (
      <div>
        <h1>Temperature Converter</h1>
        <h3>Current Scale: <span id="current-scale">{this.state.currentScale}</span></h3>
        <div className="input-container">
          <InputBox scale="Celsius"
                    value={this.state.celsius}
                    onChange={this.handleChange}
          />
          <InputBox scale="Fahrenheit"
                    value={this.state.fahrenheit}
                    onChange={this.handleChange}
          />
          <InputBox scale="Kelvin"
                    value={this.state.kelvin}
                    onChange={this.handleChange}
          />
        </div>
        <h3>Result: </h3>
        <table>
          <tr>
            <th>Celsius</th>
            <th>Fahrenheit</th>
            <th>Kelvin</th>
          </tr>
          <tr>
            <td>{this.displayValue(this.state.celsius)}</td>
            <td>{this.displayValue(this.state.fahrenheit)}</td>
            <td>{this.displayValue(this.state.kelvin)}</td>
          </tr>
        </table>
      </div>
    )
  };
}

export default Converter;