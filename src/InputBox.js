import React from 'react';

const InputBox = (props) => {
  const {scale, value, onChange} = props;
  return(
    <div className="input-box">
      <input
        name={scale}
        type="number"
        value={value}
        onChange={onChange}
        placeholder={"Enter " + scale}
      />
      <label for={scale}>{scale}</label>
    </div>
  )
};

export default InputBox;